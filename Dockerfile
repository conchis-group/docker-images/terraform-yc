FROM alpine:latest

ARG TERRAFORM_VERSION=1.6.3

RUN apk --update --no-cache add libc6-compat git openssh-client python3

# https://www.hashicorp.com/blog/installing-hashicorp-tools-in-alpine-linux-containers
RUN apk add --update --virtual .deps --no-cache gnupg && \
    cd /tmp && \
    wget https://mirror.selectel.ru/3rd-party/hashicorp-releases/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    wget https://mirror.selectel.ru/3rd-party/hashicorp-releases/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS && \
    wget https://mirror.selectel.ru/3rd-party/hashicorp-releases/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS.sig && \
    wget -qO- https://www.hashicorp.com/.well-known/pgp-key.txt | gpg --import && \
    gpg --verify terraform_${TERRAFORM_VERSION}_SHA256SUMS.sig terraform_${TERRAFORM_VERSION}_SHA256SUMS && \
    grep terraform_${TERRAFORM_VERSION}_linux_amd64.zip terraform_${TERRAFORM_VERSION}_SHA256SUMS | sha256sum -c && \
    unzip /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /tmp && \
    mv /tmp/terraform /usr/local/bin/terraform && \
    rm -f /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip terraform_${TERRAFORM_VERSION}_SHA256SUMS ${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_SHA256SUMS.sig && \
    apk del .deps

# https://cloud.yandex.ru/ru/docs/tutorials/infrastructure-management/terraform-quickstart#configure-terraform
COPY .terraformrc ~/.terraformrc

WORKDIR /terraform
