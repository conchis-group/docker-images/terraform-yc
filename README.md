# terraform-yc

[Docker Hub](https://hub.docker.com/repository/docker/conchism/terraform-yc/general)

## Usage

The image is for running terraform with yandex provider. The image is based on alpine:latest.

## Example

```
image:
    name: conchism/terraform-yc:latest
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
```

